import 'package:flutter/material.dart';
import 'package:app/Container/CallPopup.dart';
import 'package:app/Container/CustomButton.dart';
import 'package:app/Container/CustomTitleValue.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app/Container/DenyPopup.dart';
import 'package:app/Container/DetailManageCaculator.dart';

class ItemAppoinment extends StatefulWidget {
  final String title;
  final String valueSate;
  final String valueActor;
  final String valueLocation;
  final String valueCause;
  final String valueDate;
  final VoidCallback onPressed;
  const ItemAppoinment(
      {super.key,
      required this.title,
      required this.valueSate,
      required this.valueActor,
      required this.valueLocation,
      required this.valueCause,
      required this.valueDate,
      required this.onPressed});

  @override
  State<ItemAppoinment> createState() => _ItemAppoinmentState();
}

class _ItemAppoinmentState extends State<ItemAppoinment> {
  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (widget.title == 'Đã xác nhận') {
      titleColor = Color(0xFF5CBBB8);
    } else if (widget.title == 'Hủy') {
      titleColor = Color(0xFFB43939);
    } else {
      titleColor = Color(0xFF6F9BD4);
    }
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DetailManageCaculator()),
        );
      },
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: widget.title == 'Mới đăng ký' ? 200 : 150,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 3,
                  blurRadius: 10,
                  offset: Offset(0, 3),
                ),
              ],
              // color: Color(0xFFF5F5F5),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 37,
                decoration: BoxDecoration(
                  color: titleColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                ),
                child: Padding(
                    padding:
                        EdgeInsets.only(top: 8, bottom: 8, right: 12, left: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 130,
                          height: 21,
                          child: Text(
                            // title,
                            widget.title,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return CallPopup();
                                  });
                            },
                            child: SvgPicture.asset(
                              'assets/call_svg.svg',
                              width: 24,
                              height: 24,
                            ))
                      ],
                    )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                child: Container(
                  width: 300,
                  height: 100,
                  color: Colors.white,
                  child: Column(
                    children: [
                      CustomTitleValue(
                        key1: 'Trạng thái',
                        value1: widget.valueSate,
                      ),
                      CustomTitleValue(
                        key1: 'Bệnh nhân',
                        value1: widget.valueActor,
                      ),
                      CustomTitleValue(
                        key1: 'Phòng khám',
                        value1: widget.valueLocation,
                      ),
                      CustomTitleValue(
                        key1: 'Lý do khám',
                        value1: widget.valueCause,
                      ),
                      CustomTitleValue(
                        key1: 'Thời gian',
                        value1: widget.valueDate,
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 33,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomButton(
                            width: 120,
                            height: 32,
                            textButton: 'Từ chối',
                            buttonIcon: SvgPicture.asset('assets/deny.svg'),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return DenyPopup();
                                  });
                            }),
                        CustomButton(
                            width: 160,
                            height: 32,
                            textButton: 'Xác nhận lịch',
                            buttonIcon: SvgPicture.asset('assets/confirm.svg'),
                            onPressed: () {})
                      ],
                    ),
                  ),
                ),
                visible: widget.title == 'Mới đăng ký' ? true : false,
                // maintainSize: widget.title == 'Mới đăng ký'? true : false,
              )
            ],
          )),
    );
  }
}
