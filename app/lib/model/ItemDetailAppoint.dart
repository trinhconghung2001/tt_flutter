import 'package:flutter/material.dart';
import 'package:app/Container/CustomTitleDetail.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ItemDetailAppoint extends StatelessWidget {
  final String valueSate;
  final String valueStateAppoint;
  final String valueRegistry;
  final String valueObject;
  final String valueBHYT;
  final String valueName;
  final String valueBirthday;
  final String valueJob;
  final String valueSex;
  final String valuePeople;
  final String valueCountry;
  final String valueProvince;
  final String valuePhone;
  final String valueIdCart;
  final String valueCountryLiving;
  final String valueDistrict;
  final String valueCommune;
  final String valueHouse;
  final String valueRequest;
  final String valueRoom;
  final String valueDoctor;
  final String valueDate;
  final String valueContent;
  final String valueNote;

  const ItemDetailAppoint(
      {super.key,
      required this.valueSate,
      required this.valueStateAppoint,
      required this.valueRegistry,
      required this.valueObject,
      required this.valueBHYT,
      required this.valueName,
      required this.valueBirthday,
      required this.valueJob,
      required this.valueSex,
      required this.valuePeople,
      required this.valueCountry,
      required this.valueProvince,
      required this.valuePhone,
      required this.valueIdCart,
      required this.valueCountryLiving,
      required this.valueDistrict,
      required this.valueCommune,
      required this.valueHouse,
      required this.valueRequest,
      required this.valueRoom,
      required this.valueDoctor,
      required this.valueDate,
      required this.valueContent,
      required this.valueNote});

  @override
  Widget build(BuildContext context) {
    SvgPicture? svgPicture = null;
    return Column(
      children: [
        Wrap(children: [
          Container(
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            child: Column(
              children: [
                Wrap(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomTitleDetail(
                            keyItem: 'Trạng thái',
                            valueItem: valueSate,
                            // iconItem: svgPicture,
                          ),
                          CustomTitleDetail(
                            keyItem: 'Trạng thái đến khám',
                            valueItem: valueStateAppoint,
                            // iconItem: svgPicture),
                          ),
                          CustomTitleDetail(
                            keyItem: 'Kênh đăng ký (*)',
                            valueItem: valueRegistry,
                            // iconItem: svgPicture),
                          ),
                          CustomTitleDetail(
                            keyItem: 'Đối tượng (*)',
                            valueItem: valueObject,
                            // iconItem: svgPicture),
                          ),
                          CustomTitleDetail(
                            keyItem: 'Số thẻ BHYT (*)',
                            valueItem: valueBHYT,
                            // iconItem: svgPicture),
                          ),
                          CustomTitleDetail(
                            keyItem: '',
                            valueItem: 'Xác thực thẻ bhyt',
                            // iconItem: svgPicture),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                width: 300 / 3 + 30,
                                height: 20,
                                child: Text(''),
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: 300 / 3 * 2 - 30,
                                  height: 20,
                                  child: Text('Xác thực thẻ BHYT',
                                      style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          fontSize: 11,
                                          color: Color(0xFF6F9BD4))),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ]),
        SizedBox(
          width: 5,
        ),
        Container(
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            child: Column(children: [
              Wrap(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomTitleDetail(
                          keyItem: 'Họ và tên (*)',
                          valueItem: valueName,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Ngày sinh (*)',
                          valueItem: valueBirthday,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Nghề nghiệp (*)',
                          valueItem: valueJob,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Giới tính (*)',
                          valueItem: valueSex,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Dân tộc (*)',
                          valueItem: valuePeople,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Quốc tịch (*)',
                          valueItem: valueCountry,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Tỉnh khai sinh',
                          valueItem: valueProvince,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Số điện thoại (*)',
                          valueItem: valuePhone,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Chứng minh thư',
                          valueItem: valueIdCart,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Tỉnh/Thành phố (*)',
                          valueItem: valueCountryLiving,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Huyện (*)',
                          valueItem: valueDistrict,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Xã (*)',
                          valueItem: valueCommune,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Số nhà (*)',
                          valueItem: valueHouse,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ])),
        Container(
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            child: Column(children: [
              Wrap(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomTitleDetail(
                          keyItem: 'Yêu cầu khám (*)',
                          valueItem: valueRequest,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Phòng khám (*)',
                          valueItem: valueRoom,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Bác sĩ',
                          valueItem: valueDoctor,
                        ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.end,
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   children: [
                        //     CustomTitleDetail(
                        //       keyItem: 'Thời gian khám (*)',
                        //       valueItem: valueDate,
                        //     ),
                        //     Container(
                        //       margin: EdgeInsets.only(bottom: 10),
                        //       child: Icon(
                        //         Icons.date_range,
                        //         size: 12,
                        //       ),
                        //     )
                        //   ],
                        // ),
                        CustomTitleDetail(
                          keyItem: 'Nội dung khám',
                          valueItem: valueContent,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Ghi chú',
                          valueItem: valueNote,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ]))
      ],
    );
  }
}
