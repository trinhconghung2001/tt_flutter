import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app/Container/CallPopup.dart';
import 'package:app/Container/CustomButton.dart';
import 'package:app/Container/DenyPopup.dart';
import 'package:app/model/ItemDetailAppoint.dart';

class DetailManageCaculator extends StatefulWidget {
  const DetailManageCaculator({super.key});

  @override
  State<DetailManageCaculator> createState() => _DetailManageCaculatorState();
}

class _DetailManageCaculatorState extends State<DetailManageCaculator> {
  //bubbles icon call
  final GlobalKey _floatingKey = GlobalKey();
  late Size floatingSize;
  Offset floatingLocation = Offset(300, 400); // biến lưu vị trí x,y
  // lấy kích thước của widget được xác định bằng _floatingKey và gán kích thước đó cho biến floatingSize
  void getFloatingSize() {
    RenderBox _floatingBox =
        _floatingKey.currentContext!.findRenderObject() as RenderBox;
    floatingSize = _floatingBox.size;
  }

  void onDrapUpdate(BuildContext context, DragUpdateDetails details) {
    //vi tri cu chi
    final RenderBox box = context.findRenderObject()! as RenderBox;
    final Offset offset = box.globalToLocal(details.globalPosition);

    //khu vuc man hinh
    const double startX = 0;
    final double endX = MediaQuery.of(context).size.width - floatingSize.width;
    print(endX);
    final double startY = MediaQuery.of(context).padding.top;
    // final double endY = context.size?.height ?? -floatingSize.height;
    final double endY =
        MediaQuery.of(context).size.height - 115 - floatingSize.height;
    print(endY);
    //kiem tra nam trong kich thuoc phu hop k
    if (startX < offset.dx && offset.dx < endX) {
      if (startY < offset.dy && offset.dy < endY) {
        setState(() {
          floatingLocation = Offset(offset.dx, offset.dy);
        });
      }
    }

    //update vi tri
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getFloatingSize();
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    final height_screen = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          title: Text('Chi tiết lịch hẹn khám CSYT'),
          backgroundColor: Color(0xFF6F9BD4),
        ),
        body: GestureDetector(
          onVerticalDragUpdate: (DragUpdateDetails details) {
            onDrapUpdate(context, details);
          },
          onHorizontalDragUpdate: (DragUpdateDetails details) {
            onDrapUpdate(context, details);
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Stack(children: [
                Container(
                  width: width_screen,
                  height: height_screen - 130,
                  child: SingleChildScrollView(
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          child: ItemDetailAppoint(
                              valueSate: 'Đặt lịch thành công',
                              valueStateAppoint: 'Đã đến khám',
                              valueRegistry: 'Qua VNCARE',
                              valueObject: 'BHYT',
                              valueBHYT: 'DN0999999999999',
                              valueName: 'Dương Thị Mai Anh',
                              valueBirthday: '09/07/2009',
                              valueJob: 'Tri thức',
                              valueSex: 'Nữ',
                              valuePeople: 'Kinh',
                              valueCountry: 'Việt Nam',
                              valueProvince: 'Hà Nội',
                              valuePhone: '0999999999',
                              valueIdCart: '999999999',
                              valueCountryLiving: 'Hà Nội',
                              valueDistrict: 'Đống Đa',
                              valueCommune: 'Nam Từ Liêm',
                              valueHouse: '1102',
                              valueRequest: 'Khám sản',
                              valueRoom: 'Phòng khám sản 1',
                              valueDoctor: 'Nguyễn Thu Trang',
                              valueDate: '09/08/2021 09:40',
                              valueContent: 'Khám sản',
                              valueNote: 'Đã gọi xác nhận'),
                        )),
                  ),
                ),
                Positioned(
                  child: GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return CallPopup();
                          });
                    },
                    child: SvgPicture.asset(
                      'assets/call_bubbles.svg',
                      key: _floatingKey,
                    ),
                  ),
                  //set vị trí ban đầu
                  left: floatingLocation.dx,
                  top: floatingLocation.dy,
                ),
              ]),
              Container(
                width: width_screen,
                height: 35,
                margin: EdgeInsets.only(bottom: 7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomButton(
                        width: 110,
                        height: 30,
                        textButton: 'Từ chối',
                        buttonIcon: SvgPicture.asset('assets/deny.svg'),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return DenyPopup();
                              });
                        }),
                    CustomButton(
                        width: 85,
                        height: 30,
                        textButton: 'Sửa',
                        buttonIcon: SvgPicture.asset('assets/edit.svg'),
                        onPressed: () {}),
                    CustomButton(
                        width: 145,
                        height: 30,
                        textButton: 'Xác nhận lịch',
                        buttonIcon: SvgPicture.asset('assets/confirm.svg'),
                        onPressed: () {
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) => DetailManageCaculator()),
                          // );
                        })
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
